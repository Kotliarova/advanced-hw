const burgerMenu = document.querySelector('.header__burger');
const menu = document.querySelector('.header__menu');
const links = document.querySelectorAll('.header__li');


burgerMenu.addEventListener('click', ev => {
    burgerMenu.classList.toggle('header__burger_active');
    menu.classList.toggle('header__menu_active');
})