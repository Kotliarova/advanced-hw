
const { src, dest, watch, series, parallel } = require('gulp');
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const minifyjs = require('gulp-js-minify');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
const imagemin = require('gulp-imagemin');

function cleanDist() {
  // TODO: якщо папки dist немає - буде бити помилку при спробі видалення папки, пофіксити
  return src('./dist/', { read: false })
    .pipe(clean())
}

function compileCss() {
  return src('./src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename({suffix: '.min', prefix: ''}))
    .pipe(dest('./dist/'))
}

function minifyNormalize() {
  return src('./src/css/normalize.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename({suffix: '.min', prefix: ''}))
    .pipe(dest('./dist/'))
}

function compileJs() {
  return src('./src/js/**/*.js')
    .pipe(concat('scripts.js'))
    .pipe(minifyjs())
    .pipe(rename({suffix: '.min', prefix: ''}))
    .pipe(dest('./dist/'));
}

function optimizationImg() {
  return src('./src/img/*')
    .pipe(imagemin())
    .pipe(dest('./dist/img/'));
}

exports.cleanDist = cleanDist;
exports.compileCSS = compileCss;
exports.compileJs = compileJs;
exports.optimizationImg = optimizationImg;

exports.build = series(cleanDist, parallel(minifyNormalize, compileCss, compileJs), optimizationImg);

function serve() {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
  watch("./src/scss/**/*.scss", compileCss);
  watch("./src/js/**/*.js", compileJs);
  watch("./*.html").on('change', browserSync.reload);
}

exports.dev = serve;








